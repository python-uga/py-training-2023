# Python training UGA

[Repository of this training](https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/py-training-uga)

## Content

A training to acquire strong bases in Python to use it efficiently...

## The teachers/authors

- Pierre Augier: researcher at [LEGI](http://www.legi.grenoble-inp.fr/) studying
  geophysical turbulence with experiments and numerical simulations. Maintainer of
  the [FluidDyn project](https://fluiddyn.readthedocs.io);

- Cyrille Bonamy (LEGI),

- Margaux Mouchené (ISTerre),

- Mathieu Istas (LPMC),

- Loïc Huder (ESRF),

- Eric Maldonado (INRAE),

- Franck Thollard (ISTerre),

- Colin Thomas (ISTerre).

## Setup the environment for this course

One of the first step is to setup a good computing environment. However, it may be
better to first read [this introduction on Linux and Bash](part0/intro_bash.ipynb)
to understand what we are going to do.

The installation instructions are in this file: [install.md](part0/install.md).
Note that you can copy/paste commands!

## Clone this repository

Clone the repository with Mercurial (and the extension hg-git, as explained
[here](https://fluiddyn.readthedocs.io/en/latest/mercurial_heptapod.html)):

```sh
hg clone https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/py-training-uga.git
```

or with ssh (so you need to create a ssh key and copy the public key on
https://gricad-gitlab.univ-grenoble-alpes.fr):

```sh
hg clone git@gricad-gitlab.univ-grenoble-alpes.fr:python-uga/py-training-uga.git
```

## Go further

If you feel that you need more advanced content, you can work on this
[Python HPC training](https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/training-hpc)
(the associated slides are
[here](https://python-uga.gricad-pages.univ-grenoble-alpes.fr/training-hpc/)).
