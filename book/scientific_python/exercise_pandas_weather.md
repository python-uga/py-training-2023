---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Exercise with Pandas: weather

Pandas is an open source library providing high-performance, easy-to-use data
structures and data analysis tools for Python.

```{code-cell} ipython3
import pandas as pd

filename = "data_weather/synop-2016.csv"

df = pd.read_csv(filename, sep=",", encoding="utf-8", header=0)

# conversion Kelvin to °C
df.Temperature -= 273.15
```

```{code-cell} ipython3
# max temperature
df["Temperature"].max()
```

```{code-cell} ipython3
# mean temperature
df["Temperature"].mean()
```

```{code-cell} ipython3
# total rainfall
df[df["Rainfall 3 last hours"]>0]["Rainfall 3 last hours"].sum()
```

```{code-cell} ipython3
# August max temperature
df[df["Date"].str.startswith("2016-08")]["Temperature"].max()
```
