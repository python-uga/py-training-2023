---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Introduction to Matplotlib

([gallery](http://matplotlib.org/gallery.html))

The default library to plot data is `Matplotlib`. It allows one the creation of
graphs that are ready for publications with the same functionality than Matlab.

```{code-cell} ipython3
# these ipython commands load special backend for notebooks
# %matplotlib widget
%matplotlib inline
```

When running code using matplotlib, it is highly recommended to start ipython with
the option `--matplotlib` (or to use the magic ipython command `%matplotlib`).

```{code-cell} ipython3
---
slideshow:
  slide_type: slide
---
import numpy as np
import matplotlib.pyplot as plt
```

You can plot any kind of numerical data.

```{code-cell} ipython3
y = [1, 2, 10, 0, 5, 2]
plt.plot(y)
plt.show()
```

In scripts, the `plt.show` method needs to be invoked at the end of the script.

We can plot data by giving specific coordinates.

```{code-cell} ipython3
x = np.linspace(0, 2, 20)
y = x**2
```

```{code-cell} ipython3
plt.figure()
plt.plot(x, y, label="Square function")
plt.xlabel("x")
plt.ylabel("y")
plt.legend()
plt.show()
```

We can associate the plot with an object figure. This object will allow us to add
labels, subplot, modify the axis or save it as an image.

```{code-cell} ipython3
fig, ax = plt.subplots()
lines = ax.plot(
    x,
    y,
    color="red",
    linestyle="dashed",
    linewidth=3,
    marker="o",
    markersize=5,
)

ax.set_xlabel("$Re$")
ax.set_ylabel("$\Pi / \epsilon$")
plt.show()
```

We can also recover the plotted matplotlib object to get info on it.

```{code-cell} ipython3
line_object = lines[0]
type(line_object)
```

```{code-cell} ipython3
print("Color of the line is", line_object.get_color())
print("X data of the plot:", line_object.get_xdata())
```

## Example of multiple subplots

```{code-cell} ipython3
fig, axes = plt.subplots(nrows=2, ncols=1)
ax1, ax2 = axes
X = np.arange(0, 2 * np.pi, 0.1)
ax1.plot(X, np.cos(2 * X), color="red")
ax2.plot(X, np.sin(2 * X), color="magenta")
ax2.set_xlabel("Angle (rad)")
fig.tight_layout()
```

## Titles, labels and legends

Titles can be added to figures and subplots. Labels can be added when plotting to
generate a legend afterwards.

```{code-cell} ipython3
x = np.arange(0, 2, 0.01)

fig, ax = plt.subplots()
ax.plot(x, x**2, label='$x^2$')
ax.plot(x, x**3, label='$x^3$')
ax.plot(x, np.exp(x) - 1, label='$e^{x} - 1$')
ax.set_title('ax title')
ax.legend()
fig.suptitle('FIG TITLE', fontweight='bold')
plt.show()
```

Note that legends are attached to subplots. Note also the difference between the
subplot title and the title of the figure.

## Saving the figure

Figures can be saved by calling `savefig` on the `Figure` object

```{code-cell} ipython
fig.savefig('my_figure.png')
```

## Anatomy of a Matplotlib figure

![Anatomy of a figure](./anatomy.png)

For consistent figure changes, define your own stylesheets that are basically a
list of parameters to tune the aspect of the figure elements. See
https://matplotlib.org/tutorials/introductory/customizing.html for more info.


## 2D plots

There are two main methods:

- `imshow`: for square grids. X, Y are the center of pixels and (0,0) is top-left
  by default.

- `pcolormesh` (or `pcolor`): for non-regular rectangular grids. X, Y are the
  corners of pixels and (0,0) is bottom-left by default.

```{code-cell} ipython3
noise = np.random.random((10,10))

fig, axes = plt.subplots(1, 2)
axes[0].imshow(noise)
axes[1].pcolormesh(noise)
plt.show()
```

We can also add a colorbar and adjust the colormap.

```{code-cell} ipython3
plt.figure()
plt.imshow(noise, cmap=plt.cm.gray)
plt.colorbar()
plt.show()
```

### Meshgrid

When in need of plotting a 2D function, it is useful to use meshgrid that wil
generate a 2D mesh from the values of abscissas and ordinates

```{code-cell} ipython3
x = np.linspace(-2*np.pi, 2*np.pi, 200)
y = x

mesh_x, mesh_y = np.meshgrid(x, y)
Z = np.cos(2*mesh_x) + np.cos(4*mesh_y)

fig, ax = plt.subplots()
pcmesh = ax.pcolormesh(mesh_x, mesh_y, Z, cmap='RdBu')
fig.colorbar(pcmesh)
plt.show()
```

### Choose your colormaps wisely !

When doing such colorplots, it is easy to lose the interesting features by setting
a colormap that is not adapted to the data.

As a rule of thumb:

- use sequential colormaps for data varying continuously from a value to another
  (ex: `x**2` for positive values).

- use divergent colormaps for data varying around a mean value (ex: `cos(x)`).

Also, when producing scientific figures, think about how will your plot will look
like to colorblind people or in greyscales (as it can happen in printed
articles...).

See the interesting discussion on matplotlib website:
https://matplotlib.org/users/colormaps.html.

And this very important article on the scientific (mis)use of colour:
https://www.nature.com/articles/s41467-020-19160-7

## Other plot types

Matplotlib also allows to plot:

- Histograms
- Plots with error bars
- Box plots
- Contours
- in 3D
- ...

See the [gallery](http://matplotlib.org/gallery.html) to see what suits you the
most.

```{code-cell} ipython3
# 3D example
from mpl_toolkits.mplot3d import Axes3D

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

ax.plot_surface(mesh_x, mesh_y, np.exp(-(mesh_x**2+mesh_y**2)), cmap='viridis')
plt.show()
```
