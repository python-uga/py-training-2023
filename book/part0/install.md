# Setup computing and development environments with `conda`

For this training, we will use Python 3 (with [Miniforge]), a good Python IDE
(either Spyder or VSCode), Jupyter and a version-control tool (Mercurial, or Git
if you know it and really like it).

In this chapter, we give indications about how to install these tools and how to
get the repository of this training locally on your computer.

```{important}
Please, follow these instructions before the training and if you encounter problems, fill an issue
[here](https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/py-training-uga/issues)
to explain what you did and what are the errors (please copy / paste the error
log). It is also a good exercice to learn how to open an issue in an issue tracker
and how to interact to find a solution to your problem.
```

## Install good editors

Spyder or VSCode are two good open-source editors (IDE). [Spyder] is specialized
on Python and more oriented on scientific programming. VSCode is more generalist
and lightweight.

Jupyter (lab) is another program that can be used to develop software and
notebooks. Jupyter can be used online (for example
[here](https://gricad-jupyter.univ-grenoble-alpes.fr)) but it is also very
convenient to be able to install it and launch it locally.

It is good if you are able to use these different tools in the situations they are
the most adapted. Finally, you will choose you preferred tools but in practice, it
is good to be able to install and manage these software on different computers.

- https://docs.spyder-ide.org/current/installation.html

- https://code.visualstudio.com/download

- https://jupyterlab.readthedocs.io/en/stable/getting_started/installation.html

```{important}
Please try to install Spyder and VSCode from the links above
in the computer you are going to use during the training.
In contrast, we will install JupyterLab with conda in the following.
```

## Install Miniforge and Python

The first step is to install [Miniforge], which is a modified version of
[Miniconda](https://docs.conda.io/en/latest/miniconda.html) using by default the
community driven [conda-forge] channel.

You can follow
[the install instructions](https://github.com/conda-forge/miniforge). On Linux,
you need to run in a terminal

```bash
wget "https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-$(uname)-$(uname -m).sh"
bash Miniforge3-$(uname)-$(uname -m).sh -b
$HOME/miniforge3/bin/conda init bash
```

```{warning}
On windows, follow the instructions given in https://github.com/conda-forge/miniforge
and as explained in this site, do not forget to
[manually](https://learn.microsoft.com/en-us/previous-versions/office/developer/sharepoint-2010/ee537574(v=office.14)#to-add-a-path-to-the-path-environment-variable)
add the `C:\Users\myusername\miniforge3\condabin` folder to the path environment variable.
```

When it's done, open a new terminal (click on `ctrl-alt-t`) and check that the
line in the new terminal starts with `(base)`. If yes, you can close the old
terminal (with `ctrl-d`). The indication `(base)` means that you use the base
"environment".

`conda` and `mamba` are 2 commandline tools to manage software installations and
create "environments".

```{margin}
**`conda` and `mamba`: differences?**

Conda is an open source package management system and environment management
system that runs on Windows, macOS, Linux and z/OS.

Mamba is a new implementation of conda, which can be faster for some operations.
However, modern `conda` now uses internally some parts of `mamba`,
so one can just use the `conda` command line.
```

```{admonition} Definition: environment
A environment is a set of programs and libraries with particular versions. An
environment is defined by the software installed on your computer and by
environment variables, in particular the variable `$PATH`, which contains all
the paths where your shell looks for programs (you can read the output of `echo
$PATH`).
```

It is very useful to be able to create different environments for different tasks.
It is usually better to keep the `base` environment only for the `conda` software
and to use different environments for other tasks. We will use this strategy here.
We will have

- 1 environment for some basic libraries used in this course (called `main`),

- 1 environment with Mercurial (automatically created with the tool `conda-app`)

`conda` takes the programs that it installs from "channels". We are going to use
the largest open-source community driven channel called [conda-forge]. With
[Miniforge], [conda-forge] is by default the main channel.

We can start by creating the `main` environment with the commands:

```bash
conda env create --file=https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/py-training-uga/-/raw/main/main_environment.yml
```

While `conda` creates the environment, we can study the most important conda
commands:

- `conda activate main`

- `conda deactivate`

- `conda search fluidsim`

- `conda create -n name_env numpy pandas`

- `conda env list`

```{tip}

One can add the line `conda activate main` at the end of the file `~/.bashrc`.

```

## Install and setup Mercurial

To install Mercurial, you can do:

```bash
pip install conda-app -U
conda-app install mercurial
```

Close the terminal (`ctrl-d`) and reopen a new terminal (`ctrl-alt-t`)! The
command `hg` should be available.

```{warning}
Mercurial **has to be correctly setup**! Since we will use [the Gitlab
instance of UGA](https://gricad-gitlab.univ-grenoble-alpes.fr), the Mercurial
extension hg-git has to be activated so the line `hggit =` in the configuration
file `~/.hgrc` is mandatory. However, conda-app should have set this for you.

You can check that everything is right by running `hg version -v`.
```

## Clone this repository

Once everything is installed and set up, you should be able to clone the
repository of the training with:

```bash
hg clone https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/py-training-uga.git
```

Please
[tell us](https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/py-training-uga/issues)
if it does not work.

Once the repository is cloned you should have a new directory `py-training-uga`
(launch the command `ls` to see it) and you should be able to enter into it with
`cd py-training-uga`.

[conda-forge]: https://conda-forge.org/
[miniforge]: https://github.com/conda-forge/miniforge
[spyder]: https://www.spyder-ide.org/
