---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Functions (basics)

A function is a block of organized, reusable code that is used to perform a
single, related action. Functions provide better modularity for your application
and a high degree of code reusing.

## Simple function definitions and calls

Function blocks begin with the keyword `def` followed by the function name and
parentheses (`()`).

- The code block within every function starts with a colon (:) and is indented.
- Any input parameters or arguments should be placed within these parentheses.

```{code-cell} ipython3
def print_hello():
    "hello printer"
    print("hello")


def myprint(my_var):
    "my hello printer"
    print("I print", my_var)


# function calls
print_hello()
print_hello()
myprint("First call of myprint")
myprint("Second call of myprint")
```

- The first statement of a function can be the documentation string of the
  function, also called "docstring".
- The statement `return [expression]` exits a function, optionally passing back an
  expression to the caller. No return statement or a return statement with no
  arguments is the same as `return None`.

```{admonition} Duck typing

> In computer programming, duck typing is an application of the duck test—"If it walks like a duck and it quacks like a duck, then it must be a duck"—to determine whether an object can be used for a particular purpose

_[Duck typing](https://en.wikipedia.org/wiki/Duck_typing) on Wikipedia_

```

```{code-cell} ipython3
def add(arg0, arg1):
    """Print and return the sum of the two arguments (duck typing)."""
    result = arg0 + arg1
    print("result = ", result)
    return result
```

```{code-cell} ipython3
add(2, 3)
```

```{code-cell} ipython3
add("a", "b")
```

```{exercise-start}
---
label: ex-add-second-twice
---
```

Write a function that returns the sum of the first argument with twice the second
argument.

```{code-cell} ipython3
def add_second_twice(arg0, arg1):
    """Return the sum of the first argument with twice the second one.
    Arguments should be of type that support sum and product by
    an integer (e.g. numerical, string, list, ...)

    :param arg0: first argument
    :param arg1: second argument
    :return: arg0 + 2 * arg1
    """
    ...
```

```{exercise-end}
```

```{solution-start} ex-add-second-twice
---
class: dropdown
---
```

```{code-cell} ipython3
def add_second_twice(arg0, arg1):
    """Return the sum of the first argument with twice the second one.
    Arguments should be of type that support sum and product by
    an integer (e.g. numerical, string, list, ...)

    :param arg0: first argument
    :param arg1: second argument
    :return: arg0 + 2 * arg1
    """
    result = arg0 + 2 * arg1
    print(f"arg0 + 2*arg1 = {arg0} + 2*{arg1} = {result}")
    return result


assert 13 == add_second_twice(3, 5)
assert "aabbbb" == add_second_twice("aa", "bb")
assert [1, 2, 3, 4, 3, 4] == add_second_twice([1, 2], [3, 4])

add_second_twice(4, 6)
```

```{code-cell} ipython3
add_second_twice("a", "b")
```

```{solution-end}
```
