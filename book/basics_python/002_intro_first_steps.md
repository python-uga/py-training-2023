---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# First steps. Different ways to use Python

## Python: a language and some interpreters

Python is a programming language.

The most common way to execute Python code is to **interpret** it. Through misuse
of language, one says that Python is an **interpreted** language (like Bash,
Matlab, and in contrast with Fortran, C or C++). To interpret code, we need an
interpreter, i.e. a program parsing Python code and running computer instructions.

Open a terminal. There are usually different python commands: `python`, `python2`
and `python3`.

```{admonition} Python 2 / Python 3

2 languages, 2 interpreters, 2 commands

```

## How to get Python

You first need to know that there are two very popular ways to install Python
packages, `pip` and `conda`. These two tools work quite differently and are used
for different tasks so it makes sense to use both.

- `pip` is the official tool to install Python packages provided by the
  [Python Packaging Authority](https://www.pypa.io) (PyPA). It is not designed to
  install non Python dependencies. `pip` installs by default from the
  [Python Package Index](https://pypi.org/) (PyPI).

- `conda` and `mamba` are 2 open-source commandline tools to manage software
  installations and create "environments". `conda` has been provided by a company
  called Anaconda Inc in their Python/R distribution Anaconda.

```{margin}
**`conda` and `mamba`: differences?**

Conda is an open source package management system and environment management
system that runs on Windows, macOS, Linux and z/OS.

Mamba is a new implementation of conda, which is faster for some operations and
has better logging.
```

```{admonition} Definition: environment
A environment is a set of programs and libraries with particular versions. An
environment is defined by the software installed on your computer and by
environment variables, in particular the variable `$PATH`, which contains all
the paths where your shell looks for programs (you can read the output of `echo
$PATH`).

One can create/use *virtual environments*. `conda` can creates conda virtual
environments (not specific to Python) and there are also Python virtual
environments which can be create for example with a command like `python3 -m
venv venv-for-something`.

```

### Install with `conda`/`mamba`

To use `conda`/`mamba`, one has to install something like
[miniforge/mambaforge](https://github.com/conda-forge/miniforge) (as we did in
[](../part0/install.md)).

### Install just Python

- On Linux, you should have the system Python (a command `python3` corresponding
  to a file `/usr/bin/python3`).

```{admonition} Virtual environments

Some Linux distributions split Python in small bits, and it is possible to
install Python without some packages of the standard library. This is in
particular the case of the Python package `venv`, used to create virtual
environments, which is provided in Ubuntu with the Ubuntu package
`python3-venv`. Virtual environments are so important for the Python ecosystem
that one should get this package.

```

- On Windows, Python can be installed in few clicks with the Microsoft Store.

- On MacOS, one can use [Homebrew](https://brew.sh/) or install with the official
  Python installer (<https://www.python.org/downloads/>)

```{admonition} Alternative installation methods

There are of course other ways to install Python for specific tasks, for
example [pyenv](https://github.com/pyenv/pyenv) and
[guix](https://guix.gnu.org).

```

## Download the repository of this training

````{exercise}
---
label: exercise-clone
---
Download the repository of this training (you need one of the programs `git` or
`mercurial`). On Unix machines, use the commands:

```bash
hg clone https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/py-training-uga.git
cd py-training-uga/book/basics_python
ls
```

````

## Execute a script with the command `python3`

````{exercise}
---
label: exercise-exec-script
---
Run a script with `python3` (`pyfiles/helloworld.py` is *just* a text file):

```bash
cat pyfiles/helloworld.py
python3 pyfiles/helloworld.py
```

```{note}
`cat` is a Unix command that prints the content of a text file.
```

````

## Work interactively with [ipython](https://ipython.org/)

The command `ipython3` launches the program [IPython](https://ipython.org/), which
is used for interactive python.

```{exercise-start}
---
label: ex-ipython
---
```

- In IPython, you can execute a first interactive instruction:

```{code-cell} ipython3
2 + 2
```

```{code-cell} ipython3
3 / 4
```

- Run the script from ipython.

```{code-cell} ipython3
run pyfiles/helloworld.py
```

```{code-cell} ipython3
# the variable name is defined in ../pyfiles/helloworld.py
print(name)
```

- Help on an object can be displayed with the question mark (try it):

```ipython
name?
```

- Try to type "name." and to press on tab... The tab key is used to tell you how
  what you typed can be completed.

- Try to use the top and bottom arrows...

```{exercise-end}
```

## Python in an IDE ([Spyder](https://pythonhosted.org/spyder/))

Launch the application Spyder, a Python IDE (Integrated development environment).

- a good code editor with:
  - syntax coloring,
  - code analysis powered by pyflakes and pylint,
  - introspection capabilities such as code completion.
- Ipython console
- variable inspector
- ...

```{important}
It is very important to use a good editor to code in Python (for example Spyder,
emacs or vi (**with a good setup!**), or
[Visual Studio Code](https://code.visualstudio.com/docs/languages/python)).
```

## Python in the browser ([JupyterLab](https://jupyter.readthedocs.io))

The presentations of this python training are made with Jupyter (demonstration).

This is a very powerful tool to present results (see these
[examples](http://nbviewer.jupyter.org/)).

```bash
jupyter-lab
```
