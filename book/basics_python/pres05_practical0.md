---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.1
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# Practical: parsing files

## Goal

The goal of this session is to practice what we have seen in the first
presentations:

- write code in scripts,
- use ipython and execute Python programs with the command python3,
- use objects of simple types (numbers, str, list, etc.),
- index and slice,
- use loops and conditions,
- try, except,
- read and write in text files.

We will write scripts that read a file (or a set of files) with a predefined
format and compute simple quantities (sum, average, number) from the values in the
files.

## Material

You will find a bunch of files in the directory `TP/TP0_file_stats/data`.

```{exercise-start} Parsing file0.1.txt
---
label: exercise-file0.1
---
```

- look at the content of file `file0.1.txt`,
- compute basic statistics (sum, average, number) from the values.

This can be done through the following steps:

- open the file,
- iterate on the lines,
- for each line, convert its values to a float,
- update the current statistics.

````{admonition} Example of output
```bash
python3 step0.0.py
file = "../data/file0.1.txt"
# on Windows:
# file = r"..\data\file0.1.txt"
# r like "raw" = no interpretation of the special characters ("\n", "\t", etc.)
# Such r-strings are also useful when we write Latex code in Python.
nb = 78; sum = 42.46; avg = 0.54
```
````

Finally, define a function that does the processing:

```{code-cell} ipython3
def compute_stats(file_name):
    """ computes the statistics of data in file_name
    :param file_name: the name of the file to process
    :type file_name: str
    :return: the statistics
    :rtype: a tuple (number, sum, average)
    """
    pass
```

```{exercise-end}
```

```{exercise-start} Parsing file0.n.txt
---
label: exercise-file0.n
---
```

Same as step 0.1 but process many files and print file base statistics and overall
statistics:

````{admonition} Example of output
```bash
python3 step0.2.py
file = "../data/file0.1.txt"
nb = 78   ; sum = 42.46  ; avg = 0.54
file = "../data/file0.2.txt"
nb = 100  ; sum = 53.29  ; avg = 0.53
file = "../data/file0.3.txt"
nb = 25   ; sum = 12.72  ; avg = 0.51
# total over all files:
nb = 203  ; sum = 108.47 ; avg = 0.53
```
````

```{exercise-end}
```

```{exercise-start} Parse a file with comments: file_with_comment_col0.txt
---
label: exercise-file-comments
---
```

Now suppose the files contains some comments (*i.e.* lines starting with a `#`).
Adapt previous script so that we do not consider these lines (see file
`file_with_comment_col0.txt`).

````{admonition} Example of output
```bash
python3 step1.0.py
file = ../data/file_with_comment_col0.txt
nb = 100; total = 53.29; avg = 0.53
```
````

Now suppose the file contains comments in the middle of the line (see *e.g.*
`file_with_comment_anywhere.txt` that contains some comments that mainly prevent
the string to float conversion.

Adapt script 1.0 to handle this format.

````{admonition} Example of output
```bash
python3 step1.1.py
file = "../data/file_with_comment_col0.txt"
nb = 100  ; sum = 53.29  ; avg = 0.53
file = "../data/file_with_comment_anywhere.txt"
nb = 96   ; sum = 51.65  ; avg = 0.54
# total over all files:
nb = 196  ; sum = 104.93 ; avg = 0.54
```
````

```{exercise-end}
```

```{exercise-start} Parse more complicated files
---
label: exercise-complicated-files
---
```

Now suppose the files are pre-formated with lines of the form

```bash
p1=0.7742 p2=0.74973 p3=0.77751
p1=0.7493 p2=0.34762 p3=0.44521
p1=0.4261 p3=0.88275 p2=0.74016
```

- Write a function that compute statistics separately for p1, p2, p3
- *BONUS*: When parsing `file_mut_cols_with_error`, print to the screen the lines
  which contain errors.

````{admonition} Example of output
```bash
python3 step2.0.py
p1 in ../data/file_mut_cols.txt - nb: 25, sum: 12.72, avg: 0.51
p2 in ../data/file_mut_cols.txt - nb: 25, sum: 12.72, avg: 0.51
p3 in ../data/file_mut_cols.txt - nb: 25, sum: 12.72, avg: 0.51
Unexpected field p7=0.213607026802 at line 23 of ../data/file_mut_cols_with_error.txt
p1 in ../data/file_mut_cols_with_error.txt - nb: 25, sum: 12.82, avg: 0.51
p2 in ../data/file_mut_cols_with_error.txt - nb: 23, sum: 11.35, avg: 0.49
p3 in ../data/file_mut_cols_with_error.txt - nb: 23, sum: 11.69, avg: 0.51
```
````

```{exercise-end}
```
