---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.1
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# Object-oriented programming: inheritance

Python is also a Object-oriented language. Object-oriented programming is very
useful and used in many libraries so it is very nice to understand how the simple
Object-oriented mechanisms work in Python.

## Concepts

```{admonition} Object
An object is an entity that has a state and a behavior. Objects are the basic
elements of object-oriented system.
```

```{admonition} Class
Classes are "families" of objects. A class is a pattern that describes how objects
will be built.
```

## Example of problem: Simulate populations of honeybees

<img src="https://static.independent.co.uk/s3fs-public/styles/story_large/public/thumbnails/image/2013/06/05/18/web-bees-epa.jpg" title="bee" alt="image bee" width="30%">

### Hierarchy of honeybees

The "adult" bees can be:

- queen
- workers
- fertile males

Each type of adult bee have different characteristics, behaviors, activities and
tasks.

## Class definition

A class is a logical entity which contains attributes and have some behavior. When
a class is defined, we need to specify its name, its dependencies (the class
inherits from at least one other class), its attributes and its methods.

```{code-cell} ipython3
class AdultBee(object):
    kind = None
    limit_age = 50.

    def __init__(self, mother, father, tag=None):
        self.mother = mother
        self.father = father

        if tag is None:
            self.tag = (self.mother.tag, self.father.tag)
        else:
            self.tag = tag

        # age in days
        self.age = 0.
        self.living = True

    def act_and_envolve(self, duration=1):
        """Time stepping method"""
        self.age += duration
        if self.age > self.limit_age:
            self.die()

    def die(self):
        self.living = False
```

The first line states that instances of the class `AdultBee` will be Python
objects. The class `AdultBee` inherits from the class `object`.

```{note}
The first line could have been replaced by the less explicit `class AdultBee:`.
Actually, in Python 3, the classes that do not inherit explicitly from another
class automatically inherit from the class `object`.
```

## Instantiation of a class

We can create objects `AdultBee`. We say that we *instantiate objects of the class
`AdultBee`*.

```{code-cell} ipython3
bee0 = AdultBee('mother0', 'father0', tag='0')
bee1 = AdultBee('mother1', 'father1', tag='1')

bee_second_generation0 = AdultBee(bee0, bee1)
bee_second_generation1 = AdultBee(bee0, bee1)

bee_third_generation = AdultBee(
    bee_second_generation0, bee_second_generation1)

bees = [bee0, bee1, bee_second_generation0, bee_second_generation1, bee_third_generation]
```

In this example, we manipulate the notions of class, object (instance),
abstraction and encapsulation...

````{admonition} Syntax to create an object
```python
bee2 = AdultBee('mother2', 'father2', tag='2')
```
````

### What happens...

- the Python object is first created
- the object is initialized, i.e. the method `__init__` is automatically called
  like this (for `bee0`):

```python
AdultBee.__init__(bee0, 'mother0', 'father0', tag='0')
```

### Special methods and attributes

In Python, methods or attributes that starts with `__` are "special". Such methods
and attributes are used internally by Python. They define how the class works
internally.

For example the method `__init__` is automatically called by Python during the
instantiation of an object with the arguments used for the instantiation.

### Protected methods and attributes (no notion of `public`, `private`, `virtual` as in C++)

Attributes and methods whose names start with `_` are said to be "protected". It
is just a name convention. It tells the users that they should not use these
objects directly.

```{admonition} Warning for C++ users

`__init__` is NOT the constructor. The real constructor is `__new__`. This method
is called to really create the Python object and it really returns the object.
Usually, we do not need to redefine it. Python `__init__` and C++ constructor have
to be used in very different ways. Only the `__init__` method of the class is
automatically called by Python during the instantiation. Nothing like the Russian
dolls C++ mechanism.

```

### Use the objects (instances)

```{code-cell} ipython3
print('second generation:', bee_second_generation0.tag)
print('third generation; ', bee_third_generation.tag)
print('warning: consanguinity...')
```

```{code-cell} ipython3
# 100 days
for i in range(100):
    for bee in bees:
        bee.act_and_envolve()
    bees = [bee for bee in bees if bee.living]

if len(bees) == 0:
    print('After 100 days, no more bees :-(')
```

## Inheritance

To indicate the dependency to an other class, we put the parent class in
parenthesis in the definition. The class `QueenBee` inherits from the class
`AdultBee`

```{code-cell} ipython3
class QueenBee(AdultBee):
    kind = 'queen'
    limit_age = 4*365

    def act_and_envolve(self, duration=1):
        """Time stepping method"""
        super().act_and_envolve(duration)
        print('I am the Queen!')

class WorkerBee(AdultBee):
    kind = 'worker'
    # actually it depends on the season...
    limit_age = 6*7
    def dance(self):
        print('I communicate by dancing')
    def make_honey(self):
        print('I make honey')
```

- The methods that are not rewritten are automatically inherited from the parent
  class.
- The methods that are rewritten are completely replaced. To use the method of the
  parent class, it has to be called explicitly (for example with the `super()`
  function).

We see that we do not need to rewrite all methods. For example the method
`__init__` of the class `QueenBee` is the method `__init__` of the class
`AdultBee`.

The class `AdultBee` that we defined is also derived from a more generic class
that is called `object`. Let's check the content of the class `QueenBee`.

```{code-cell} ipython3
queen = QueenBee('mother0', 'father0', tag='0')

print(dir(queen))
```

All the methods that star with the prefix `__` are inherited from the class
`object`. All classes in Python3 inherit from `object`.

```{code-cell} ipython3
queen.act_and_envolve()
```

### `super` function

We have used the function `super()` like this to call a function of the parent
class:

```python
super().act_and_envolve(duration)
```

````{note}
The python 2 syntax was more complicated. We would have to write:
```python
super(QueenBee, self).act_and_envolve(duration)
```
````

````{note}
We can also call the method explicitly:
```
AdultBee.act_and_envolve(self, duration)
```
````

## Remark: the exceptions are classes...

We can define our own exceptions classes inheriting from an exception class.

```{code-cell} ipython3
class MyValueError(ValueError):
    pass

def myfunc():
    raise MyValueError('oops')

try:
    myfunc()
except OSError:
    print('An OSError was raised')
except ValueError:
    print('A ValueError was raised')
```

## Static methods and class methods (advanced)

### "Class methods"

When we simply define a method in a class, it is a instance method, i.e. the first
argument of the method (`self`) points toward the instance used to call the
method. This is the normal and most common mechanism.

We could also define methods that work for the class using the decorator
`@classmethod`:

```{code-cell} ipython3
class Person(object):
    def __init__(self):
        pass

class Student(Person):
    role = 'student'
    @classmethod
    def show_role(cls):
        print('The role for this class is ' +
              cls.role + '.')

Student.show_role()
```

### "Static methods"

For some situation we don't even need to explicitly use the class or an instance.
We can use static methods.

```{code-cell} ipython3
class IdPerson(Person):
    count = 0
    def __init__(self, name):
        self.name = name
        self.id = IdPerson.count
        IdPerson.count += 1

    @staticmethod
    def show_nb_person():
        print('Number of persons created: ', IdPerson.count)
```

```{code-cell} ipython3
p1 = IdPerson('Pierre')
p2 = IdPerson('Cyrille')
p3 = IdPerson('Olivier')
p4 = IdPerson('Franck')

IdPerson.show_nb_person()
```

```{exercise-start} Using inheritance for our weather station problem
---
label: oop-weather
---
```

At the end of the last presentation, we asked the following question about our
weather stations measuring wind and temperature:

> What if we now have a weather station that also measure humidity ? Do we have to
> rewrite everything ?

Give your own answer by doing the following tasks:

- Write a class `HumidWeatherStation` inheriting `WeatherStation` (code reproduced
  below) to implement a new attribute to store the humidity measurements.

- Write a function `humidity_at_max_temp` that returns the value of the humidity
  at the maximal temperature. Use the fact that `HumidWeatherStation` inherits
  from `WeatherStation` and therefore can use the method `arg_max_temp` previously
  implemented !

- *Advanced*: Overloadg the methods of `WeatherStation` to take humidity into
  account when computing percieved temperatures `Tp`. For simplicity, we will
  assume that `Tp = Tw + 5*humidity` with `Tw` the temperature computed with the
  wind chill effect.

```{code-cell} ipython3
class WeatherStation(object):
    """ A weather station that holds wind and temperature """

    def __init__(self, wind, temperature):
        """ initialize the weather station.
        Precondition: wind and temperature must have the same length
        :param wind: any ordered iterable
        :param temperature: any ordered iterable"""
        self.wind = list(wind)
        self.temp = list(temperature)
        if len(self.wind) != len(self.temp):
            raise ValueError(
                "wind and temperature should have the same size"
            )

    def perceived_temp(self, index):
        """ computes the perceived temp according to
        https://en.wikipedia.org/wiki/Wind_chill
        i.e. The standard Wind Chill formula for Environment Canada is:
        apparent = 13.12 + 0.6215*air_temp - 11.37*wind_speed^0.16 + 0.3965*air_temp*wind_speed^0.16

        :param index: the index for which the computation must be made
        :return: the perceived temperature"""
        air_temp = self.temp[index]
        wind_speed = self.wind[index]
        # Perceived temperature does not have a sense without wind...
        if wind_speed == 0:
            apparent_temp = air_temp
        else:
            apparent_temp = 13.12 + 0.6215*air_temp \
                            - 11.37*wind_speed**0.16 \
                            + 0.3965*air_temp*wind_speed**0.16
        # Let's round to the integer to avoid trailing decimals...
        return round(apparent_temp,0)

    def perceived_temperatures(self):
        """ Returns an array of percieved temp computed from the temperatures and wind speed data """
        apparent_temps = []
        for index in range(len(self.wind)):
            # Reusing the method perceived_temp defined above
            apparent_temperature = self.perceived_temp(index)
            apparent_temps.append(apparent_temperature)
        return apparent_temps

    def max_temp(self, perceived=False):
        """ returns the maximum temperature record in the station"""
        if perceived:
            apparent_temp = self.perceived_temperatures()
            return max(apparent_temp)
        else:
            return max(self.temp)

    def arg_max_temp(self, perceived=False):
        """ returns the index of (one of the) maximum temperature record in the station"""
        if perceived:
            temp_array_to_search = self.perceived_temperatures()
        else:
            temp_array_to_search = self.temp
        return temp_array_to_search.index(self.max_temp(perceived))
```

```{exercise-end}
```

```{solution-start} oop-weather
---
class: dropdown
---
```

```{code-cell} ipython3
class HumidWeatherStation(WeatherStation):
    """ A weather station that holds wind, temperature and humidity. Inherits from WeatherStation """

    def __init__(self, wind, temperature, humidity):
        """ initialize the weather station.
        Precondition: wind, temperature and humidity must have the same length
        :param wind: any ordered iterable
        :param temperature: any ordered iterable
        :param humidity: any ordered iterable"""
        # Delegate the initialisation of wind and temperature to the mother class constructor
        super(HumidWeatherStation, self).__init__(wind, temperature)
        # Or: super().init(wind, temperature)

        # Add humidity treatement
        self.humidity = [x for x in humidity]
        if len(self.humidity) != len(self.temp):
            raise ValueError("humidity and temperature should have the same size")
        # If humidity and temp have the same size, humidity and wind do as well
        # as len(temp) == len(wind) is enforced from the mother class constructor

    def humidity_at_max_temp(self):
        """ Returns the value of humidity at the maximal temperature
        """
        index_max_temp = self.arg_max_temp()
        return self.humidity[index_max_temp]

    def perceived_temp(self, index):
        """ Compute the perceived temperature according to wind_speed (wind-chill) and humidity

        :param index: the index for which the computation must be made
        :return: the perceived temperature"""
        # Compute the wind-chilled temp from WeatherStation method
        wind_chilled_temp = super().perceived_temp(index)
        apparent_temp = wind_chilled_temp + 5*self.humidity[index]
        return round(apparent_temp, 2)

    def perceived_temps(self):
        """ Returns an array of percieved temp computed from the temperatures, wind speed and humidity data """
        apparent_temps = []
        for index in range(len(self.temp)):
            # This time, we use the perceived_temp method of HumidWeatherStation
            apparent_temperature = self.perceived_temp(index)
            apparent_temps.append(apparent_temperature)
        return apparent_temps

singapore = HumidWeatherStation(wind=[11, 23, 23, 19, 18, 18],
                           temperature = [28, 33, 31, 32, 35, 34],
                           humidity = [0.78, 0.63, 0.61, 0.58, 0.5, 0.72])
print(singapore.humidity_at_max_temp()) #0.5 expected
print(singapore.max_temp()) #35 expected
# As we overloaded perceived_temp, the rest of the class features work with the new behaviour
print(singapore.perceived_temps())
print(singapore.max_temp(perceived=True))
```

In this case, we used inheritance to create the new object (`HumidWeatherStation`)
to:

- Add new features (`humidity_at_max_temp`) to an existing object without
  rewritting the common features
- Define new behaviours for features already present (`perceived_temp`) that
  integrate well with the structure in place

For such needs, think about inheritance.

```{solution-end}
```

```{exercise} Moving objects, bikes, cars and pollution
---
label: oop-moving-objects
---
- Write a class named `MovingObject` that has at least one attribute `position`
and implements two functions `start()` and `stop()`. These 2 functions could
just print for example "starting" and "stopping" (or they could do more funny
things)...

- Write other classes `Car` and `Bike` which inherit `MovingObject` and
overload the `start` and `stop` methods.

- Use the classes (instantiate objects and use them). Compute the total
pollution after some times.

```

```{solution-start} oop-moving-objects
---
class: dropdown
---
```

```{code-cell} ipython3
# a solution
pollution = 0.

class MovingObject:

    def __init__(self, position=0., max_speed=1., acceleration=1.):
        self.position = position
        self.max_speed = max_speed
        self.acceleration = acceleration

    def start(self):
        print ("starting")

    def stop(self):
        print ("stoping")


class Car(MovingObject):
    count = 0

    def __init__(self, position=0., max_speed=1., acceleration=1., name='', pollution_start=0.5):
        super(Car, self).__init__(position, max_speed, acceleration)
        self.name = name
        self.pollution_start = pollution_start
        Car.count += 1

    def start(self):
        global pollution
        print ('The car ' + self.name + ' starts (vrooum)')
        pollution += self.pollution_start

    def stop(self):
        print ('The car ' + self.name + ' stops')

    @staticmethod
    def get_number_of_cars():
        print ("There are " + str(Car.count) + " cars")

class Bike(MovingObject):
    pass

ferrari = Car(name='Ferrari')
mybike = Bike()
mybike.name = 'blue bike'
porsche = Car(name='Porsche', pollution_start=0.8)

ferrari.start()
ferrari.stop()
porsche.start()
porsche.stop()

objs = [ferrari, porsche, mybike]
for f in objs:
    f.start()

print(f'pollution at the end: {pollution}')
```

```{solution-end}
```
