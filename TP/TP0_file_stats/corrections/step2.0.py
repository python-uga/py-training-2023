#!/usr/bin/env python3


def compute_p1p2p3_stats(file_name):
    """
    Computes the statistics of data in a file stored in 3 fields.
    Each field of the form key=val where key is p1, p2 or p3, and val is a float.

    :param file_name: the name of the file to process
    :type file_name: str
    :return: A tuple containing the statistics for each field under the form of a tuple (number, sum, average)
    """
    p1_sum = 0.0
    p2_sum = 0.0
    p3_sum = 0.0

    p1_number = 0
    p2_number = 0
    p3_number = 0
    with open(file_name) as handle:
        for i, line in enumerate(handle):
            if line.startswith("#"):
                continue

            fields = line.strip().split()
            for field in fields:
                if field.startswith("p1="):
                    p1_sum = p1_sum + float(field[3:])
                    p1_number = p1_number + 1
                    continue

                if field.startswith("p2="):
                    p2_sum = p2_sum + float(field[3:])
                    p2_number = p2_number + 1
                    continue

                if field.startswith("p3="):
                    p3_sum = p3_sum + float(field[3:])
                    p3_number = p3_number + 1
                    continue

                print(f"Unexpected field {field} at line {i} of {file_name}")

    return (
        (p1_number, p1_sum, p1_sum / p1_number),
        (p2_number, p2_sum, p2_sum / p2_number),
        (p3_number, p3_sum, p3_sum / p3_number),
    )


file_names = ["../data/file_mut_cols.txt", "../data/file_mut_cols_with_error.txt"]

for file_name in file_names:
    p1_result, p2_result, p3_result = compute_p1p2p3_stats(file_name)
    print(
        f"p1 in {file_name} - nb: {p1_result[0]}, sum: {p1_result[1]:.2f}, avg: {p1_result[2]:.2f}"
    )
    print(
        f"p2 in {file_name} - nb: {p2_result[0]}, sum: {p2_result[1]:.2f}, avg: {p2_result[2]:.2f}"
    )
    print(
        f"p3 in {file_name} - nb: {p3_result[0]}, sum: {p3_result[1]:.2f}, avg: {p3_result[2]:.2f}"
    )
