#!/usr/bin/env python3
"""Computes basic statistics on file that contains a set of lines, each line
containing one float.
"""

file_name = '../data/file0.1.txt'

my_sum = 0.0
number = 0

with open(file_name) as handle:
    for line in handle:
        elem = float(line)
        my_sum = my_sum + elem
        number += 1

# not formatted output
# print('nb={}, sum={}, avg={}'.format(number, sum, sum/float(number)))

# formatted output
print((f'file = "{file_name}\nnb = {number}; sum = {my_sum:.2f};'
       f'avg = {my_sum/number:.2f}'))
