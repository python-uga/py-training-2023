#!/usr/bin/env python3
"""Computes basic statistics on file that contains a set of lines, each line
containing one float and possibly some comments in the middle of the line.

"""


def compute_stats(file_name):
    """
    computes the statistics of data in file_name

    :param file_name: the name of the file to process
    :type file_name: str
    :return: the statistics
    :rtype: a tuple (number, sum, average)
    """
    sum_ = 0.0
    number = 0
    with open(file_name) as file:
        for line in file:
            if line.startswith('#'):
                continue
            if '#' in line:
                line = line.split('#', 1)[0]
            elem = float(line)
            sum_ += elem
            number += 1

    return number, sum_, float(sum_ / number)


file_names = [
    "../data/file_with_comment_col0.txt",
    "../data/file_with_comment_anywhere.txt",
]

numbers = []
sums = []

for file_name in file_names:
    len_file, sum_file, avg_file = compute_stats(file_name)
    numbers.append(len_file)
    sums.append(sum_file)

    print(
        f'file = "{file_name}"\nnb = {len_file:5}; '
        f"sum = {sum_file:7.2f}; avg = {sum_file / len_file:5.2f}"
    )


all_sum = sum(sums)
all_numbers = sum(numbers)
all_avg = all_sum/all_numbers
print('# total over all files:\n'
      f'nb = {all_numbers}; sum = {all_sum:.2f}; avg = {all_avg:.2f}')
