

def compute_stat(path):

    nb_numbers = 0
    mysum = 0.

    with open(path) as f:
        for line in f:
            if line.strip().startswith('#'):
                continue
            if '#' in line:
                line = line.split()[0]
            number = float(line)
            nb_numbers += 1
            mysum += number

    average = mysum / nb_numbers

    return mysum, nb_numbers, average


if __name__ == '__main__':

    fmt = 'data/file{}.txt'

    files = [fmt.format(s) for s in ['1', '1.1', '1.2', '2', '2.1']]
    results = []

    sum_tot = 0.
    nb_values_tot = 0

    for path in files:
        mysum, nb_numbers, average = compute_stat(path)
        print(('file {}\n'.format(path) +
               'mysum =      {}\n'
               'nb_numbers = {}\n'
               'average =    {}\n' + 16 * '-').format(
                   mysum, nb_numbers, average))
        results.append((mysum, nb_numbers, average))

        sum_tot += mysum
        nb_values_tot += nb_numbers

    average = sum_tot / nb_values_tot
    print(('sum_tot =       {}\n'
           'nb_values_tot = {}\n'
           'average =       {}').format(sum_tot, nb_values_tot, average))
