

path = 'data/file_mut_cols.txt'

path = 'data/file_mut_cols_with_error.txt'

startings = ['p{}='.format(i) for i in range(1, 4)]

with open(path) as f:
    for line in f:
        words = line.split()
        if len(words) != 3:
            print(line)

        if any([not word.startswith(start) for word, start in
                zip(words, startings)]):
            print(line)
