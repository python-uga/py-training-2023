import numpy as np
import matplotlib.pyplot as plt
import imageio

filename = 'data/img_010.jpg'
im = imageio.imread(filename)
print(f"image size: {im.shape}")
plt.figure()
plt.imshow(im)
plt.show()
