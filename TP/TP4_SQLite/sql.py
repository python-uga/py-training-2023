# -*- coding: utf-8 -*-
"""
Created on Mon Jan 30 21:43:59 2017

@author: eric
"""

import sqlite3


class Movie:

    def __init__(self, id, name, year):
        self.id = id
        self.name = name
        self.year = year


def create_db():
    db_loc = sqlite3.connect('movie.db')
    cursor = db_loc.cursor()
    cursor.execute('drop table if exists movie')
    cursor.execute('drop table if exists actor')
    cursor.execute('drop table if exists role')
    cursor.execute('create table if not exists movie \
                    (id integer primary key,name varchar(40), year integer)')
    cursor.execute('create table if not exists actor \
                    (id integer primary key,name varchar(40), \
                    firstname varchar(40),year integer)')
    cursor.execute('create table if not exists role \
                    (id_movie integer,id_actor integer)')
    db_loc.commit()
    db_loc.close()


def load_db():
    db_loc = sqlite3.connect('movie.db')
    cursor = db_loc.cursor()
    with open('movies.txt') as f:
        for line in f:
            values = line.strip('\n').split(";")
            query = 'insert into movie values(' + values[0] + ',\'' \
                    + values[1] + '\',' + values[2] + ')'
            cursor.execute(query)
            db_loc.commit()
    f.close()

    with open('actors.txt') as f:
        for line in f:
            values = line.strip('\n').split(";")
            query = 'insert into actor values(' + values[0] + ',\'' \
                    + values[1] + '\',\'' + values[2] + '\',' + values[3] \
                    + ')'
            cursor.execute(query)
            db_loc.commit()
    f.close()

    with open('roles.txt') as f:
        for line in f:
            values = line.strip('\n').split(";")
            query = 'insert into role values(' + values[0] + ',' + values[1] \
                    + ')'
            cursor.execute(query)
            db_loc.commit()
    f.close()

    db_loc.close()


def insert_a_movie(theMovie):
    query = 'insert into movie values(' + str(theMovie.id) + ',\'' \
            + theMovie.name + '\',' + str(theMovie.year) + ')'
    print (query)
    cursor.execute(query)
    db_loc.commit()

# create db and load data
create_db()
load_db()

db_loc = sqlite3.connect('movie.db')
cursor = db_loc.cursor()

# add a movie with the movie object
theMovie = Movie(12, 'Back to the Future', 1985)
insert_a_movie(theMovie)

# display all movies
cursor.execute('SELECT * FROM movie;')
movies = cursor.fetchall()
for movie in movies:
    print(movie[1])

# display steve buscemi's movies
cursor.execute('SELECT distinct(movie.name) FROM movie,actor , role \
                where movie.id=role.id_movie \
                and role.id_actor=actor.id \
                and actor.name=\'Buscemi\'')
movies = cursor.fetchall()
for movie in movies:
    print(movie[0])
db_loc.close()
