import torch
import torch.nn as nn

# Define the MLP model with ReLU and Dropout
class Single_layer_perceptron(nn.Module):
    def __init__(self, input_size, hidden_size, output_size, activation_function):
        super(Single_layer_perceptron, self).__init__()
        self.input_size = input_size

        self.hidden_layer = nn.Linear(input_size*input_size, hidden_size)
        self.non_linear_activation = activation_function

        self.output_layer = nn.Linear(hidden_size, output_size)

    # Define the forward function, given an image as entry, should return
    # a probability for each label
    def forward(self, x):
        # x has a shape batch_sizex 1 x 28 x 28, it must be flatten to batch_size x 1 x 784 instead

        x = x.reshape(-1, 1 * self.input_size * self.input_size)

        hidden_output = self.hidden_layer(x)
        hidden_output = self.non_linear_activation(hidden_output)

        # Pass the hidden layer output through the output layer, which is usually just a linear layer
        y_pred = self.output_layer(hidden_output)
        return y_pred

# Set device (GPU if available, otherwise CPU)
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# Define the number of neurons for the hidden layer, typically a power of 2
num_neurons = 32

# Create an instance of the custom neural network
pixel_size = 28 # input size
output_size = 10  # output size
activation_function = torch.relu
single_layer = Single_layer_perceptron(pixel_size, num_neurons, output_size, activation_function)
# transfer the model to the gpu (if there is one)
single_layer = single_layer.to(device)

# specify loss function
loss_function = nn.CrossEntropyLoss()

# specify optimizer and learning rate
# a strong learning rate means the weights are updated a lot at each iteration, 
# however, a too big learning rate will make you miss a narrow minimum
# If you see strong oscillations in error, it is too big, if nothing changes in a few epochs it is too small
learning_rate = 0.02
optimizer_single_layer = torch.optim.SGD(single_layer.parameters(), lr=learning_rate)