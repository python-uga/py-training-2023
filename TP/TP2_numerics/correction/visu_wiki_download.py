#!/usr/bin/env python3

"""
Load content of a wiki log file, extract the meaningful column
and save it to pickle file.
"""

import sys
import time
import argparse
import os
import json
import numpy as np
import matplotlib.pyplot as plt

def load_dict(file_name):
    """ loads a dictionnary from a file.
        supposed format: json

    :file_name: the input file name
    :returns: the loaded dictionnary

    """
    dic = {}
    with open(file_name, "r") as _handle:
        dic = json.load(_handle)
    return dic



def sort_dict_files(directory):
    """Reads a directory and provide an list of json files
       with name of the form wiki.NUMBER.json and return the
       list of files ordered numerically by NUMBER

    :param directory: the directory to consider
    :type directory: str
    :returns: the ordered list of file names
    :rtype: list of str
    """
    # different ways to build the list of sorted integers (namely out)
    # short version one:
    # return sorted([int(x.replace("wiki.", "").replace(".json", "")) for x in
    # os.listdir(directory) if x.endswith(".json") and x.startswith("wiki.")])

    # shot version two with regex
    # return sorted([int(re.sub("(^wiki\.|\.json$)", "",x) for x in os.listdir(directory)
    #               if  re.match("^wiki\.\d+\.json$")])
    # long version
    out = []
    for fname in os.listdir(directory):
        if fname.startswith("wiki.") and fname.endswith(".json"):
            int_fname = int(fname.replace("wiki.", "").replace(".json", ""))
            out.append(int_fname)
    return ["{}/wiki.{}.json".format(directory, x) for x in sorted(out)]


def load_dics(directory):
    """ loads the files from a given directory
    sorted by the numbers

    :param directory: directory that contains the json files
    :returns: a list of directories
    """
    return [load_dict(x) for x in sort_dict_files(directory)]

def extract_countries(dicts):
    """Extract the set of countries, ie the union of all the 2 letters keys
    :dicts: the list of dicts.
    :returns:  the sorted union of countries
    """
    union = set()
    for s in (set(d.keys()) for d in dicts):
        union.update(s)
    return sorted(list(union))

def select_country(list_dict, country):
    """ build the statistics of country out of list_dict

    :list_dict: the ordered list of statistics dictionnaries: list_dict[i]
    is the dictionnary of the form {'es':100, 'fr':78, 'en':1000} for timestamps
    i.
    :country: a country to select
    :returns:  the list of statistics for country, e.g. ret[i] is 78 if country was 'fr'.
    """
    stat_country = []
    for dic in list_dict:
        if country in dic:
            stat_country.append(dic[country])
        else:
            stat_country.append(-1)
    return stat_country


def build_matrix_from_dicts(dics):
    """ Build and return a matrix of the form
    one line per country, one count per column.
    M(x,y) will countain the query count for country at
    line x, at timestamps y

    :dics: a list of dictionnary, ordered by timestamps
    :returns: a numpy 2D matrix
    """
    countries = extract_countries(dics)
    return np.array([select_country(dics, x) for x in countries]), countries

def select_countries(mat, country_sel, all_countries):
    """select the country selection out of mat and provide a matrix
       reduced to the set of selected countries

    :mat: the whole matrix
    :country_sel: the list of country of interest
    :returns: the selected sub matrix
    """
    countries_pos = [all_countries.index(c) for c in country_sel]
    return mat[countries_pos]

def show_countries(mat, country_names):
    """ build a stack plot for the given elements

    :mat: the countryXtimestamps matrix
    :country_names: the array of names (ie the mapping between idx and name)
    """
    ind = np.arange(mat.shape[1])
    # building a mapping contry['name'] = pos in mat
    fig, ax = plt.subplots()
    ax.set_title("Number of queries per hour to wikipedia")
    ax.set_xlabel("Timestamps (one point = one hour)")
    ax.set_ylabel("#queries per hour")
    for idx in range(mat.shape[0]):
        ax.plot(ind, mat[idx], linewidth=1, label=country_names[idx])
    ax.legend(loc='lower right')
    plt.show()

def compute_l2_dist(vect1, vect2):
    """ computes the l2 distance between two vectors

    :vect1: first vector
    :type vect1: numpy array
    :vect2: second vector
    :returns:  the distance (L2)
    """
    # computes \sum_i (x_i-y_i)^2 = \sum_i x_i^2 + \sum_i y_i^2 - 2 * \sum_i x_i*y_i
    xi2 = np.dot(vect1, vect1)
    yi2 = np.dot(vect2, vect2)
    cross_p = np.dot(vect1, vect2)
    return xi2 + yi2 - 2 * cross_p

def	compute_l2_matrix(mat):
    """ computes the L2 distance matrix row wise,
    in the result, res[i,j] = L2(row_i, row_j) = L2(country_i, country_j)

    :mat: the input matrix.
    :result: the distance matrix row wise.
    """

    three_sums = np.sum(np.square(mat)[:, np.newaxis,:], axis=2) -\
                 2 * mat.dot(mat.T) + \
                 np.sum(np.square(mat), axis=1)
    dist = np.sqrt(three_sums)
    return dist




if __name__ == "__main__":
    # no argument given -> we add -h so that the help will be triggered
    if len(sys.argv) == 1:
       sys.argv.append('-h')
    parser = argparse.ArgumentParser(description=("read the set of dictionnary and do some"
                                                  "visualisations. Dictionnaries are provide"
                                                  "in json format"))
    parser.add_argument("-d", type=str, help="the directory where input dictionnaries are")
    parser.add_argument("-c", type=str, help="do statistics on a given country")
    args = parser.parse_args()

    # load dictionnaries
    dic_list = load_dics(args.d)
    # build countries of interest
    cois = args.c.split(",")
    mat_countries, countries_names = build_matrix_from_dicts(dic_list)
    mat_sel = select_countries(mat_countries, cois, countries_names)
    show_countries(mat_sel, cois)
