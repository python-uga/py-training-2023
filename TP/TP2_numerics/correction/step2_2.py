#!/usr/bin/env python3
"""
"""

import pickle
import json
import os.path
from glob import glob

import numpy as np


def pickle_load_dict(infile):
    """ load a pickle file and save it to a dictonnary
    :param infile : the input file name
    :type infile : str
    :param dictionnary: the dictionnary that wil contain data
    :type dictionnary: dict
    """
    with open(infile, 'rb') as _handle:
        dictionnary = pickle.load(_handle)
    _handle.close()

    return dictionnary


def json_load_dict(infile):
    """ load a json file and save it to a dictonnary
    :param infile : the input file name
    :type infile : str
    :param dictionnary: the dictionnary that wil contain data
    :type dictionnary: dict
    """
    with open(infile, 'r') as _handle:
        dictionnary = json.load(_handle)
    _handle.close()

    return dictionnary


def create_array():
    """ create a numpy array from json dictionnaries
    """
    # intialize empty list to store dictionnaries
    data = []

    # go trough all the json file of the directory and save the
    # content to the list
    pattern = os.path.join('./json_dicts', '*.json')
    for file_name in glob(pattern):
        data.append(json_load_dict(file_name))

    # get the unique keys across entire dataset
    keys = [list(dx.keys()) for dx in data]

    # flatten and coerce to 'set'
    keys = {itm for inner_list in keys for itm in inner_list}

    # create a map (look-up table) from each key
    # to a column in a NumPy array

    table = dict(enumerate(keys))

    # pre-allocate NUmPy array (100 rows is arbitrary)
    # number of columns is len(table.keys())
    mat = np.empty((len(data), len(table.keys())))

    keys = list(table.keys())
    # now populate the array from the original data using table
    for i, row in enumerate(data):
        mat[i, :] = [row.get(table[k], 0) for k in keys]

if __name__ == "__main__":
    create_array()
