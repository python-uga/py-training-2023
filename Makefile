SHELL := /bin/bash

html:
	time pdm run build-book

install-deps: sync

sync:
	pdm sync --clean

pdf:
	pdm run build-pdf

clean:
	pdm run clean

format:
	mdformat README.md book
	black book --extend-exclude book/basics_python/pyfiles/wrong.py

lock:
	pdm lock
