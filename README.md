# Python training UGA

**A training to acquire strong bases in Python to use it efficiently**

The code of this training has been written for Python 3.

Formated output of the notebooks is available here:
https://python-uga.gricad-pages.univ-grenoble-alpes.fr/py-training-uga

## Get the repository

The repository hosted in the Gitlab of UGA:
https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga

- Clone the repository with Git:

```sh
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/py-training-uga.git
# or with ssh
git clone git@gricad-gitlab.univ-grenoble-alpes.fr:python-uga/py-training-uga.git
```

- or clone the repository with Mercurial (and the extension
  [hg-git](https://foss.heptapod.net/mercurial/hg-git)), with the same commands
  but replacing `git` by `hg`:

```sh
hg clone https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/py-training-uga.git
# or with ssh
hg clone git@gricad-gitlab.univ-grenoble-alpes.fr:python-uga/py-training-uga.git
```

## Authors

Pierre Augier (LEGI), Cyrille Bonamy (LEGI), Eric Maldonado (INRAE), Franck
Thollard (ISTERRE), Mathieu Istas (ISTERRE), Margaux Mouchène (ISTERRE), Loïc
Huder (ESRF)

## Build the book

```sh
python3 -m pip install pipx
python3 -m pipx ensurepath
# in a new terminal
pipx install pdm
pdm sync --clean
pdm run build-book
```

The produced files can be removed with `pdm run clean`.

It's convenient to activate the virtual environment produced by PDM. In Bash, one
can run `. .venv/bin/activate`.

## For writers of this course

To display and/or modify the presentations, use the Makefile. See:

```sh
make help
```

mdformat (and black) are used to format the sources of the book (command
`make format`). This package can be installed for example with `pipx`:

```sh
pipx install mdformat-myst --include-deps
```
